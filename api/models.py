from django.db import models


class Alarm(models.Model):
    time = models.DateTimeField()
    text = models.TextField()
    notified = models.BooleanField(default=False)
