from django.db.models.functions import Now
from rest_framework import generics

from api.models import Alarm
from api.serializers import AlarmSerializer


class AlarmList(generics.ListCreateAPIView):
    serializer_class = AlarmSerializer
    queryset = Alarm.objects.filter(time__gt=Now())
