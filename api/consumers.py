import asyncio

from channels.db import database_sync_to_async
from channels.generic.websocket import AsyncJsonWebsocketConsumer
from django.db.models.functions import Now

from api.models import Alarm
from api.serializers import AlarmSerializer


class AlarmConsumer(AsyncJsonWebsocketConsumer):
    async def connect(self):
        await self.accept()

        # start observing alarms
        while True:
            data = await self.get_expired_alarms()
            if data is not None:
                await self.send_json(data)
            await asyncio.sleep(1)

    def disconnect(self, close_code):
        pass

    # return serialized just expired alarms
    @database_sync_to_async
    def get_expired_alarms(self):
        alarms = Alarm.objects.filter(time__lt=Now(), notified=False)

        if alarms.exists():
            serializer = AlarmSerializer(alarms, many=True)
            data = serializer.data

            # set as notified to prevent repeated notifications
            alarms.update(notified=True)

            return data
        else:
            return None
